package com.example.jlmpx.contextpractices.activities;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.jlmpx.contextpractices.R;
import com.example.jlmpx.contextpractices.context.Context;
import com.example.jlmpx.contextpractices.context.ContextManager;


public class ContextAwareActivity extends Activity {

    private ContextManager mContextManager;
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mContextManager = ContextManager.getInstance(this);
        mContextManager.startContextAcquisition();
        mContext = mContextManager.getContext();

        mContextManager.printContext();

        mContextManager.observeContext();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mContextManager.stopContextAcquisition();
    }

    public void setMessage(String s) {
        TextView tvMessage = (TextView) findViewById(R.id.messages);
        tvMessage.setText(s);
    }

    public void setTextAxis(String x, String y, String z) {
        TextView tvAxisX = (TextView) findViewById(R.id.x_axis);
        TextView tvAxisY = (TextView) findViewById(R.id.y_axis);
        TextView tvAxisZ = (TextView) findViewById(R.id.z_axis);
        tvAxisX.setText(x);
        tvAxisY.setText(y);
        tvAxisZ.setText(z);
    }

    public void setLuminosity(String s) {
        TextView tvLight = (TextView) findViewById(R.id.light);
        tvLight.setText(s);
    }

    public void setImageView(int x) {
        ImageView ivAccelAxis = (ImageView) findViewById(R.id.image);
        ivAccelAxis.setImageResource(x);
    }

    public void setLuminosityLevel(String s, int i) {
        TextView tvLightLevel = (TextView) findViewById(R.id.reading);
        tvLightLevel.setText(s);

        ProgressBar pbLightLevel = (ProgressBar) findViewById(R.id.lightmeter);
        pbLightLevel.setProgress(i);
    }
}
