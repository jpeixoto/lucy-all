package com.example.jlmpx.contextpractices.interfaces.listeners;

import com.example.jlmpx.contextpractices.interfaces.ContextElement;
import com.example.jlmpx.contextpractices.context_model.ContextData;

public interface ContextElementListener {

    void onContextElementChanged(ContextElement contextElement, ContextData data);
}
