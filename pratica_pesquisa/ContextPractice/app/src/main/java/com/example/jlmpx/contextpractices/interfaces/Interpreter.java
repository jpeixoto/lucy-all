package com.example.jlmpx.contextpractices.interfaces;

public interface Interpreter extends ContextElement{

    void interpret(Widget widget);
}
