package com.example.jlmpx.contextpractices.widgets;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import com.example.jlmpx.contextpractices.context.ContextType;
import com.example.jlmpx.contextpractices.interfaces.Widget;
import com.example.jlmpx.contextpractices.interfaces.listeners.ContextElementListener;
import com.example.jlmpx.contextpractices.context_model.ContextData;
import com.example.jlmpx.contextpractices.utils.ContextDataMaker;

import java.util.ArrayList;

public class Accelerometer implements Widget, SensorEventListener{

    private float accelX, accelY, accelZ;
    private float accuracy;

    private SensorManager mSensorManager;

    private ArrayList<ContextElementListener> mListeners;

    public Accelerometer(){
        mListeners = new ArrayList<>();
    }

    @Override
    public void start(Context context) {

        mSensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        Sensor mAccelSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        mSensorManager.registerListener(this, mAccelSensor, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    public void stop() {
        if(mSensorManager != null){
            mSensorManager.unregisterListener(this);
        }
    }

    @Override
    public ContextData getContextData() {
        return createAccelData();

    }

    @Override
    public ContextType getType() {
        return ContextType.ACCELEROMETER;
    }

    @Override
    public void addContextElementListener(ContextElementListener listener) {
        if(!mListeners.contains(listener)){
            mListeners.add(listener);
        }
    }

    @Override
    public void removeContextElementListener(ContextElementListener listener) {
        if(mListeners.contains(listener)){
            mListeners.remove(listener);
        }
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        accelX = event.values[0];
        accelY = event.values[1];
        accelZ = event.values[2];

        notifyAllListeners(createAccelData());
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        this.accuracy = accuracy;
    }

    private ContextData createAccelData() {
        float timestamp = System.currentTimeMillis();
        return ContextDataMaker.makeAccelData(accelX, accelY, accelZ, accuracy, timestamp);
    }

    private void notifyAllListeners(ContextData accelData) {
        for(ContextElementListener listener : mListeners){
            listener.onContextElementChanged(this, accelData);
        }
    }
}
