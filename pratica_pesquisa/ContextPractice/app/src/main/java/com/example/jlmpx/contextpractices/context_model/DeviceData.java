package com.example.jlmpx.contextpractices.context_model;

/**
 * Created by jlmpx on 16/10/2016.
 */

public class DeviceData {
    public static class AccelAxisData extends ContextData{

        public enum AccelAxis {X, Y, Z, NONE}

        private AccelAxis axis;

        public AccelAxis getAxis() {
            return axis;
        }

        public void setAxis(AccelAxis axis) {
            this.axis = axis;
        }
    }

    public static class AccelData extends ContextData{

        private float x, y, z;

        public float getX() {
            return x;
        }

        public void setX(float x) {
            this.x = x;
        }

        public float getY() {
            return y;
        }

        public void setY(float y) {
            this.y = y;
        }

        public float getZ() {
            return z;
        }

        public void setZ(float z) {
            this.z = z;
        }
    }
}
