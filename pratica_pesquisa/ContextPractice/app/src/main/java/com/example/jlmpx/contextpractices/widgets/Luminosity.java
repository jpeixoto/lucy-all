package com.example.jlmpx.contextpractices.widgets;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import com.example.jlmpx.contextpractices.context.ContextType;
import com.example.jlmpx.contextpractices.interfaces.Widget;
import com.example.jlmpx.contextpractices.interfaces.listeners.ContextElementListener;
import com.example.jlmpx.contextpractices.context_model.ContextData;
import com.example.jlmpx.contextpractices.utils.ContextDataMaker;

import java.util.ArrayList;

public class Luminosity implements Widget, SensorEventListener{

    private float luminosity;
    private float accuracy;

    private SensorManager mSensorManager;

    private ArrayList<ContextElementListener> mListeners;

    public Luminosity(){
        mListeners = new ArrayList<>();
    }

    @Override
    public void start(Context context) {

        mSensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        Sensor mLumiSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);

        mSensorManager.registerListener(this, mLumiSensor, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    public void stop() {
        if(mSensorManager != null){
            mSensorManager.unregisterListener(this);
        }
    }

    @Override
    public ContextData getContextData() {
        return createLumiData();

    }

    @Override
    public ContextType getType() {
        return ContextType.LUMINOSITY;
    }

    @Override
    public void addContextElementListener(ContextElementListener listener) {
        if(!mListeners.contains(listener)){
            mListeners.add(listener);
        }
    }

    @Override
    public void removeContextElementListener(ContextElementListener listener) {
        if(mListeners.contains(listener)){
            mListeners.remove(listener);
        }
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        luminosity = event.values[0];

        notifyAllListeners(createLumiData());
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        this.accuracy = accuracy;
    }

    private ContextData createLumiData() {
        float timestamp = System.currentTimeMillis();
        return ContextDataMaker.makeLumiData(luminosity, accuracy, timestamp);
    }

    private void notifyAllListeners(ContextData lumiData) {
        for(ContextElementListener listener : mListeners){
            listener.onContextElementChanged(this, lumiData);
        }
    }
}
