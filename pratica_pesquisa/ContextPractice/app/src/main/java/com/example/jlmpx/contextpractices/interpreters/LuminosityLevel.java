package com.example.jlmpx.contextpractices.interpreters;

import com.example.jlmpx.contextpractices.context.ContextType;
import com.example.jlmpx.contextpractices.context_model.PlaceData;
import com.example.jlmpx.contextpractices.interfaces.ContextElement;
import com.example.jlmpx.contextpractices.interfaces.Interpreter;
import com.example.jlmpx.contextpractices.interfaces.Widget;
import com.example.jlmpx.contextpractices.interfaces.listeners.ContextElementListener;
import com.example.jlmpx.contextpractices.context_model.ContextData;
import com.example.jlmpx.contextpractices.utils.ContextDataMaker;

import java.util.ArrayList;

public class LuminosityLevel implements Interpreter, ContextElementListener{

    private Widget luminosity;

    private PlaceData.LumiLevelData.LumiLevel level;
    private PlaceData.LumiLevelData.LumiLevel lastLevel;
    private float luminosityValue;
    private float accuracy;

    private ArrayList<ContextElementListener> mListeners;

    public LuminosityLevel(){
        mListeners = new ArrayList<>();

        // Default level
        level = PlaceData.LumiLevelData.LumiLevel.NONE;
        lastLevel = PlaceData.LumiLevelData.LumiLevel.NONE;
    }

    @Override
    public void interpret(Widget widget) {
        luminosity = widget;

        luminosity.addContextElementListener(this);
    }

    @Override
    public ContextData getContextData() {
        return createLumiLevelData();
    }

    @Override
    public ContextType getType() {
        return ContextType.LUMINOSITY_LEVEL;
    }

    @Override
    public void addContextElementListener(ContextElementListener listener) {
        if(!mListeners.contains(listener)){
            mListeners.add(listener);
        }
    }

    @Override
    public void removeContextElementListener(ContextElementListener listener) {
        if(mListeners.contains(listener)){
            mListeners.remove(listener);
        }
    }

    @Override
    public void onContextElementChanged(ContextElement contextElement, ContextData data) {
        luminosityValue = ((PlaceData.LumiData) data).getLuminosity();
        accuracy = data.getAccuracy();

        if(luminosityValue == 0){
            level = PlaceData.LumiLevelData.LumiLevel.NONE;
        }
        else if(luminosityValue <= 10){
            level = PlaceData.LumiLevelData.LumiLevel.LOW;
        }
        else if(luminosityValue <= 70){
            level = PlaceData.LumiLevelData.LumiLevel.HIGH;
        }
        else{
            level = PlaceData.LumiLevelData.LumiLevel.VERY_HIGH;
        }

        if(level != lastLevel){
            notifyAllListeners(createLumiLevelData());
            lastLevel = level;
        }
    }

    private ContextData createLumiLevelData() {
        float timestamp = System.currentTimeMillis();
        return ContextDataMaker.makeLumiLevelData(luminosityValue, level, accuracy, timestamp);
    }

    private void notifyAllListeners(ContextData lumiData) {
        for(ContextElementListener listener : mListeners){
            listener.onContextElementChanged(this, lumiData);
        }
    }

}
