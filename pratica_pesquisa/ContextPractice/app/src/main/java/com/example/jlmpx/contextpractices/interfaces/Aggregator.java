package com.example.jlmpx.contextpractices.interfaces;

import java.util.ArrayList;

public interface Aggregator extends ContextElement{

    void aggregate(ArrayList<Widget> widgets);
}
