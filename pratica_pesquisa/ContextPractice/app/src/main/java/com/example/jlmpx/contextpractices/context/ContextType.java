package com.example.jlmpx.contextpractices.context;

public enum ContextType {
    ACCELEROMETER("Accelerometer"),
    LUMINOSITY("Luminosity"),
    GPS("GPS"),
    LUMINOSITY_LEVEL("Luminosity Level"),
    ACCELEROMETER_AXIS("Accelerometer Axis"),
    USER_SECURITY("User Security");

    private String name;

    ContextType(String name){
        this.name = name;
    }

    public String getName(){
        return name;
    }
}
