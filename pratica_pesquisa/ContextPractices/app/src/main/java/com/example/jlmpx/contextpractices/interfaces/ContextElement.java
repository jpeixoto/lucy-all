package com.example.jlmpx.contextpractices.interfaces;

import com.example.jlmpx.contextpractices.context.ContextType;
import com.example.jlmpx.contextpractices.interfaces.listeners.ContextElementListener;
import com.example.jlmpx.contextpractices.context_model.ContextData;

public interface ContextElement {

    ContextData getContextData();
    ContextType getType();

    void addContextElementListener(ContextElementListener listener);
    void removeContextElementListener(ContextElementListener listener);
}
