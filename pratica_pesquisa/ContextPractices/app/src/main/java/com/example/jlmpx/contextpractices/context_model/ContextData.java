package com.example.jlmpx.contextpractices.context_model;

public class ContextData {

    private float timestamp;
    private float accuracy;

    public float getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(float timestamp) {
        this.timestamp = timestamp;
    }

    public float getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(float precision) {
        this.accuracy = precision;
    }
}
