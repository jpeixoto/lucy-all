package com.example.jlmpx.contextpractices.context;

import android.app.AlertDialog;
import android.content.DialogInterface;

import com.example.jlmpx.contextpractices.R;
import com.example.jlmpx.contextpractices.activities.ContextAwareActivity;
import com.example.jlmpx.contextpractices.aggregators.UserSecurity;
import com.example.jlmpx.contextpractices.context_model.ContextData;
import com.example.jlmpx.contextpractices.context_model.DeviceData;
import com.example.jlmpx.contextpractices.context_model.PlaceData;
import com.example.jlmpx.contextpractices.interfaces.Aggregator;
import com.example.jlmpx.contextpractices.interfaces.ContextElement;
import com.example.jlmpx.contextpractices.interfaces.Widget;
import com.example.jlmpx.contextpractices.interfaces.listeners.ContextElementListener;
import com.example.jlmpx.contextpractices.interpreters.AccelerometerAxis;
import com.example.jlmpx.contextpractices.interpreters.LuminosityLevel;
import com.example.jlmpx.contextpractices.widgets.Accelerometer;
import com.example.jlmpx.contextpractices.widgets.Luminosity;

import java.util.ArrayList;

public class ContextManager implements ContextElementListener {

    private static ContextManager instance;

    private boolean isPopupShowing = false;

    private android.content.Context androidContext;

    private ContextAwareActivity contextAwareActivity;

    private Context mContext;

    private ContextManager(android.content.Context context) {
        androidContext = context;
    }

    public static ContextManager getInstance(android.content.Context context) {
        if (instance == null) {
            instance = new ContextManager(context);
        }
        return instance;
    }

    public Context getContext() {
        return mContext;
    }

    public void startContextAcquisition() {
        // Creating the widgets
        Widget accel = new Accelerometer();
        Widget lumi = new Luminosity();

        // Creating the aggregators
        Aggregator userSecurity = new UserSecurity();
        ArrayList<Widget> securityWidgets = new ArrayList<>();
        securityWidgets.add(accel);
        securityWidgets.add(lumi);
        userSecurity.aggregate(securityWidgets);

        // Creating the interpreters
        AccelerometerAxis accelAxis = new AccelerometerAxis();
        accelAxis.interpret(accel);
        LuminosityLevel lumiLevel = new LuminosityLevel();
        lumiLevel.interpret(lumi);

        // Creating the context
        mContext = new Context();
        mContext.addContextElement(accel);
        mContext.addContextElement(lumi);
        mContext.addContextElement(accelAxis);
        mContext.addContextElement(lumiLevel);
        mContext.addContextElement(userSecurity);

        // Start the context acquisition
        accel.start(androidContext);
        lumi.start(androidContext);
    }

    public void stopContextAcquisition() {
        // Stop the context acquisition
        for (ContextElement contextElement : mContext.getContextElements()) {
            if (contextElement instanceof Widget) {
                ((Widget) contextElement).stop();
            }
        }
    }

    public void printContext() {
        String contextString = "Active Context Elements: ";
        for (ContextElement element : mContext.getContextElements()) {
            contextString += element.getType().getName() + ", ";
        }
        contextString = contextString.substring(0, contextString.length() - 2);
        contextAwareActivity.setMessage(contextString);
    }

    public void observeContext() {
        for (ContextElement contextElement : mContext.getContextElements()) {
            contextElement.addContextElementListener(this);
        }
    }

    @Override
    public void onContextElementChanged(ContextElement contextElement, final ContextData data) {
        final ContextType type = contextElement.getType();

        if (type == ContextType.ACCELEROMETER) {
            contextAwareActivity.setTextAxis(String.format("%.02f", ((DeviceData.AccelData) data).getX()),
                    String.format("%.02f", ((DeviceData.AccelData) data).getY()),
                    String.format("%.02f", ((DeviceData.AccelData) data).getZ()));
        } else if (type == ContextType.LUMINOSITY) {
            contextAwareActivity.setLuminosity(String.format("%.02f", ((PlaceData.LumiData) data).getLuminosity()));
        } else if (type == ContextType.ACCELEROMETER_AXIS) {
            DeviceData.AccelAxisData.AccelAxis axis = ((DeviceData.AccelAxisData) data).getAxis();
            if (axis == DeviceData.AccelAxisData.AccelAxis.X) {
                contextAwareActivity.setImageView(R.drawable.x);
            } else if (axis == DeviceData.AccelAxisData.AccelAxis.Y) {
                contextAwareActivity.setImageView(R.drawable.y);
            } else if (axis == DeviceData.AccelAxisData.AccelAxis.Z) {
                contextAwareActivity.setImageView(R.drawable.z);
            }
        } else if (type == ContextType.LUMINOSITY_LEVEL) {
            contextAwareActivity.setLuminosityLevel("Current light level is: " + ((PlaceData.LumiLevelData) data).getLevel().getName(),
                    (int) ((PlaceData.LumiLevelData) data).getLuminosity());
        } else if (type == ContextType.USER_SECURITY) {
            if (!isPopupShowing) {
                final AlertDialog alertDialog = new AlertDialog.Builder(contextAwareActivity).create();
                alertDialog.setTitle("Alert");
                alertDialog.setMessage("The place is getting dangerous. Run, Forrest, Run!");
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                isPopupShowing = false;
                            }
                        });
                alertDialog.show();
                isPopupShowing = true;
            }
        } else {
            // TODO Check if this is an error.
        }
    }
}
