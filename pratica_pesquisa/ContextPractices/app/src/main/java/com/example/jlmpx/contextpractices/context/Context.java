package com.example.jlmpx.contextpractices.context;

import com.example.jlmpx.contextpractices.interfaces.ContextElement;

import java.util.ArrayList;

public class Context {

    private ArrayList<ContextElement> contextElements;

    public Context(){
        contextElements = new ArrayList<>();
    }

    public void addContextElement(ContextElement contextElement){
        contextElements.add(contextElement);
    }

    public void removeContextElement(ContextElement contextElement){
        if(contextElements.contains(contextElement)){
            contextElements.remove(contextElement);
        }
    }

    public ArrayList<ContextElement> getContextElements(){
        // returning a copy of the context elements
        return new ArrayList<>(contextElements);
    }

}
