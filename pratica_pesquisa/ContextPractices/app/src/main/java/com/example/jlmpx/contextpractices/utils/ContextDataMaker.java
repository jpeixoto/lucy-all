package com.example.jlmpx.contextpractices.utils;

import com.example.jlmpx.contextpractices.context_model.DeviceData;
import com.example.jlmpx.contextpractices.context_model.PlaceData;

public class ContextDataMaker {

    public static DeviceData.AccelData makeAccelData(float x, float y, float z, float accuracy, float timestamp){

        DeviceData.AccelData data = new DeviceData.AccelData();
        data.setX(x);
        data.setY(y);
        data.setZ(z);
        data.setAccuracy(accuracy);
        data.setTimestamp(timestamp);

        return data;
    }

    public static PlaceData.LumiData makeLumiData(float luminosity, float accuracy, float timestamp){

        PlaceData.LumiData data = new PlaceData.LumiData();
        data.setLuminosity(luminosity);
        data.setAccuracy(accuracy);
        data.setTimestamp(timestamp);

        return data;
    }

    public static PlaceData.LumiLevelData makeLumiLevelData(float luminosity, PlaceData.LumiLevelData.LumiLevel level, float accuracy, float timestamp){

        PlaceData.LumiLevelData data = new PlaceData.LumiLevelData();
        data.setLuminosity(luminosity);
        data.setLevel(level);
        data.setAccuracy(accuracy);
        data.setTimestamp(timestamp);

        return data;
    }

    public static DeviceData.AccelAxisData makeAccelAxisData(DeviceData.AccelAxisData.AccelAxis axis, float accuracy, float timestamp){

        DeviceData.AccelAxisData data = new DeviceData.AccelAxisData();
        data.setAxis(axis);
        data.setAccuracy(accuracy);
        data.setTimestamp(timestamp);

        return data;
    }

    public static PlaceData.UserSecurityData makeUserSecurityData(boolean safety, float accuracy, float timestamp){

        PlaceData.UserSecurityData data = new PlaceData.UserSecurityData();
        data.setSafety(safety);
        data.setAccuracy(accuracy);
        data.setTimestamp(timestamp);

        return data;
    }
}
