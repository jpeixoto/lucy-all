package com.example.jlmpx.contextpractices.context_model;

/**
 * Created by jlmpx on 16/10/2016.
 */

public class PlaceData{
    public static class UserSecurityData extends ContextData{

        private boolean safety;

        public boolean getSafety() {
            return safety;
        }

        public void setSafety(boolean safety) {
            this.safety = safety;
        }
    }

    public static class LumiLevelData extends ContextData{

        public enum LumiLevel {NONE("None"), LOW("Low"), HIGH("High"), VERY_HIGH("Very High");

            private String name;

            LumiLevel(String name){
                this.name = name;
            }

            public String getName(){
                return name;
            }
        }

        private LumiLevel level;

        private float luminosity;

        public float getLuminosity() {
            return luminosity;
        }

        public void setLuminosity(float luminosity) {
            this.luminosity = luminosity;
        }

        public LumiLevel getLevel() {
            return level;
        }

        public void setLevel(LumiLevel level) {
            this.level = level;
        }

    }

    public static class LumiData extends ContextData{

        private float luminosity;

        public float getLuminosity() {
            return luminosity;
        }

        public void setLuminosity(float luminosity) {
            this.luminosity = luminosity;
        }
    }
}
