package com.example.jlmpx.contextpractices.aggregators;

import com.example.jlmpx.contextpractices.context.ContextType;
import com.example.jlmpx.contextpractices.context_model.DeviceData;
import com.example.jlmpx.contextpractices.context_model.PlaceData;
import com.example.jlmpx.contextpractices.interfaces.Aggregator;
import com.example.jlmpx.contextpractices.interfaces.ContextElement;
import com.example.jlmpx.contextpractices.interfaces.Widget;
import com.example.jlmpx.contextpractices.interfaces.listeners.ContextElementListener;
import com.example.jlmpx.contextpractices.context_model.ContextData;
import com.example.jlmpx.contextpractices.utils.ContextDataMaker;

import java.util.ArrayList;

public class UserSecurity implements Aggregator, ContextElementListener{

    private final static float xThreshold = 9;

    private float luminosity;
    private float x;

    private float lastX = 0.00f; // just to eliminate variation on the first measure

    private boolean safety;

    private ArrayList<ContextElementListener> mListeners;

    public UserSecurity(){
        mListeners = new ArrayList<>();

        safety = true;
    }

    @Override
    public void aggregate(ArrayList<Widget> widgets) {
        Widget accel = null;
        Widget lumi = null;

        for(Widget currentWidget : widgets){
            if(currentWidget.getType() == ContextType.ACCELEROMETER){
                accel = currentWidget;
            }
            else if(currentWidget.getType() == ContextType.LUMINOSITY){
                lumi = currentWidget;
            }
            else{
                // TODO Check if this is an error.
            }
        }

        accel.addContextElementListener(this);
        lumi.addContextElementListener(this);
    }

    @Override
    public ContextData getContextData() {
        return createUserSecurityData();
    }

    @Override
    public ContextType getType() {
        return ContextType.USER_SECURITY;
    }

    @Override
    public void addContextElementListener(ContextElementListener listener) {
        if(!mListeners.contains(listener)){
            mListeners.add(listener);
        }
    }

    @Override
    public void removeContextElementListener(ContextElementListener listener) {
        if(mListeners.contains(listener)){
            mListeners.remove(listener);
        }
    }

    @Override
    public void onContextElementChanged(ContextElement contextElement, ContextData data) {
        if(contextElement.getType() == ContextType.ACCELEROMETER){
            x = ((DeviceData.AccelData) data).getX();
        }
        else if(contextElement.getType() == ContextType.LUMINOSITY){
            luminosity = ((PlaceData.LumiData) data).getLuminosity();
        }
        else{
            // TODO Check if this is an error.
        }

        applyRules();
    }

    private void applyRules() {
        // Do not process rules on the first measure
        if(lastX == 0.00f){
            lastX = x;
            return;
        }

        float deltaX = Math.abs(x - lastX);

        if(deltaX > xThreshold && luminosity <= 10){
            safety = false;
            notifyAllListeners(createUserSecurityData());
        }

        lastX = x;
    }

    private ContextData createUserSecurityData() {
        float timestamp = System.currentTimeMillis();
        return ContextDataMaker.makeUserSecurityData(safety, xThreshold, timestamp);
    }

    private void notifyAllListeners(ContextData lumiData) {
        for(ContextElementListener listener : mListeners){
            listener.onContextElementChanged(this, lumiData);
        }
    }
}
