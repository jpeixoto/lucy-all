package com.example.jlmpx.contextpractices.interfaces;

import android.content.Context;

public interface Widget extends ContextElement{

    void start(Context context);
    void stop();
}
