package com.example.jlmpx.contextpractices.interfaces;

public interface Actuator {

    void doCommand();
    void doDefaultCommand();
}
