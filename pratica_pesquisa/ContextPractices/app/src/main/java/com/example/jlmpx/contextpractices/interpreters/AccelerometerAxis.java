package com.example.jlmpx.contextpractices.interpreters;

import com.example.jlmpx.contextpractices.context.ContextType;
import com.example.jlmpx.contextpractices.context_model.DeviceData;
import com.example.jlmpx.contextpractices.interfaces.ContextElement;
import com.example.jlmpx.contextpractices.interfaces.Interpreter;
import com.example.jlmpx.contextpractices.interfaces.Widget;
import com.example.jlmpx.contextpractices.interfaces.listeners.ContextElementListener;
import com.example.jlmpx.contextpractices.context_model.ContextData;
import com.example.jlmpx.contextpractices.utils.ContextDataMaker;

import java.util.ArrayList;

public class AccelerometerAxis implements Interpreter, ContextElementListener{

    private static final float NOISE = 2;

    private Widget accelerometer;

    private DeviceData.AccelAxisData.AccelAxis axis;
    private DeviceData.AccelAxisData.AccelAxis lastAxis;
    private float accuracy;

    private ArrayList<ContextElementListener> mListeners;

    public AccelerometerAxis(){
        mListeners = new ArrayList<>();

        // Default level
        axis = DeviceData.AccelAxisData.AccelAxis.X;
        lastAxis = DeviceData.AccelAxisData.AccelAxis.X;
    }

    @Override
    public void interpret(Widget widget) {
        accelerometer = widget;

        accelerometer.addContextElementListener(this);
    }

    @Override
    public ContextData getContextData() {
        return createAccelAxisData();
    }

    @Override
    public ContextType getType() {
        return ContextType.ACCELEROMETER_AXIS;
    }

    @Override
    public void addContextElementListener(ContextElementListener listener) {
        if(!mListeners.contains(listener)){
            mListeners.add(listener);
        }
    }

    @Override
    public void removeContextElementListener(ContextElementListener listener) {
        if(mListeners.contains(listener)){
            mListeners.remove(listener);
        }
    }

    @Override
    public void onContextElementChanged(ContextElement contextElement, ContextData data) {
        float accelX = Math.abs(((DeviceData.AccelData) data).getX());
        float accelY = Math.abs(((DeviceData.AccelData) data).getY());
        float accelZ = Math.abs(((DeviceData.AccelData) data).getZ());
        accuracy = data.getAccuracy();

        if(accelX < NOISE && accelY < NOISE && accelZ < NOISE){
            axis = DeviceData.AccelAxisData.AccelAxis.NONE;
        }
        else if(accelX > accelY && accelX > accelZ ){
            axis = DeviceData.AccelAxisData.AccelAxis.X;
        }
        else if(accelY > accelX && accelY > accelZ){
            axis = DeviceData.AccelAxisData.AccelAxis.Y;
        }
        else{
            axis = DeviceData.AccelAxisData.AccelAxis.Z;
        }

        if(axis != lastAxis){
            notifyAllListeners(createAccelAxisData());
            lastAxis = axis;
        }
    }

    private ContextData createAccelAxisData() {
        float timestamp = System.currentTimeMillis();
        return ContextDataMaker.makeAccelAxisData(axis, accuracy, timestamp);
    }

    private void notifyAllListeners(ContextData lumiData) {
        for(ContextElementListener listener : mListeners){
            listener.onContextElementChanged(this, lumiData);
        }
    }
}
