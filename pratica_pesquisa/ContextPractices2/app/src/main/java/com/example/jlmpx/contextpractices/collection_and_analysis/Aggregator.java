package com.example.jlmpx.contextpractices.collection_and_analysis;

import android.app.AlertDialog;
import android.content.DialogInterface;

public class Aggregator {
    private Widget widget;

    public Aggregator(Widget widget) {
        this.widget = widget;
    }

    public void Agg() {
        //Alert feature aggregator
        float z = 0;
        float Noise = widget.Noise;
        float mLastZ = widget.mLastZ;
        float deltaZ = Math.abs(mLastZ - z);

        float currentReading = widget.currentReading;
        final AlertDialog alertDialog = new AlertDialog.Builder(widget).create();
        if ((currentReading <= 10) && (deltaZ > Noise)) {
            alertDialog.setTitle("Alert");
            alertDialog.setMessage("The place is getting dangerous. Run, Forrest, Run!");
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            alertDialog.show();
        }
    }
}
