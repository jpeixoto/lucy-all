package com.example.jlmpx.contextpractices.collection_and_analysis;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jlmpx.contextpractices.R;

public class Widget extends Activity implements SensorEventListener {
    //Acc variables
    public float mLastX, mLastY, mLastZ;
    public boolean mInitialized;
    public SensorManager mSensorManager;
    public Sensor mAccelerometer;
    public float Noise = 2;

    //LightSensor variables
    public float currentReading;
    public Sensor mLightSensor;

    private Interpreter mInterpreter;
    private Aggregator mAggregator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mInterpreter = new Interpreter(this);
        mAggregator = new Aggregator(this);
        SensorSide sensor = new SensorSide();
        TextView textView = (TextView) findViewById(R.id.messages);
        textView.setText(sensor.logCat);

        //Accelerometer startup
        mInitialized = false;
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);

        //LightSensor startup
        mLightSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);

        if (mLightSensor == null) {
            Toast.makeText(Widget.this,
                    "No Light Sensor! quit-",
                    Toast.LENGTH_LONG).show();
        } else {
            mSensorManager.registerListener(lightSensorEventListener,
                    mLightSensor,
                    SensorManager.SENSOR_DELAY_NORMAL);
        }
    }

    public void setTextReading(String s) {
        TextView textReading = (TextView) findViewById(R.id.reading);
        textReading.setText(s);
    }

    public void setLightMeter(int i) {
        ProgressBar lightMeter = (ProgressBar) findViewById(R.id.lightmeter);
        lightMeter.setProgress(i);
    }

    public void setImageView(int x) {
        ImageView iv = (ImageView) findViewById(R.id.image);
        iv.setImageResource(x);
    }

    //Delimitation of the LightSensor
    SensorEventListener lightSensorEventListener
            = new SensorEventListener() {

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
            // TODO Auto-generated method stub
        }

        @Override
        public void onSensorChanged(SensorEvent event) {
            currentReading = event.values[0];

            // TODO Auto-generated method stub
            if (event.sensor.getType() == Sensor.TYPE_LIGHT) {
                TextView textValue = (TextView) findViewById(R.id.light);

                //light sensor widgets information
                textValue.setText("Light sensor: " + String.valueOf(currentReading));

                //light sensor interpreter information
                mInterpreter.interLightSensor();
            }
        }
    };

    //Delimitation of the Accelerometer
    @TargetApi(Build.VERSION_CODES.CUPCAKE)
    protected void onResume() {
        super.onResume();
        mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @TargetApi(Build.VERSION_CODES.CUPCAKE)
    protected void onPause() {
        super.onPause();
        mSensorManager.unregisterListener(this);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        TextView tvX = (TextView) findViewById(R.id.x_axis);
        TextView tvY = (TextView) findViewById(R.id.y_axis);
        TextView tvZ = (TextView) findViewById(R.id.z_axis);
        float x = event.values[0];
        float y = event.values[1];
        float z = event.values[2];

        if (!mInitialized) {
            mLastX = x;
            mLastY = y;
            mLastZ = z;
            tvX.setText("0.0");
            tvY.setText("0.0");
            tvZ.setText("0.0");
            mInitialized = true;
        } else {
            float deltaX = Math.abs(mLastX - x);
            float deltaY = Math.abs(mLastY - y);
            float deltaZ = Math.abs(mLastZ - z);
            if (deltaX < Noise)
                deltaX = (float) 0.0;
            if (deltaY < Noise)
                deltaY = (float) 0.0;
            if (deltaZ < Noise)
                deltaZ = (float) 0.0;
            mLastX = x;
            mLastY = y;
            mLastZ = z;
            tvX.setText(Float.toString(deltaX));
            tvY.setText(Float.toString(deltaY));
            tvZ.setText(Float.toString(deltaZ));

            //accelerometer aggregator information
            mInterpreter.interAcc();

            //aggregator information
            mAggregator.Agg();
        }

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

}
