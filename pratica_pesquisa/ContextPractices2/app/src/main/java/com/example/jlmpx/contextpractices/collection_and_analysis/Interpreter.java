package com.example.jlmpx.contextpractices.collection_and_analysis;


import com.example.jlmpx.contextpractices.R;

public class Interpreter {

    private Widget widget2;

    public Interpreter(Widget widget) {
        this.widget2 = widget;
    }

    /* float currentReading;*/
    public void interLightSensor() {
        float currentReading = widget2.currentReading;
        widget2.setLightMeter((int) currentReading);

        if (currentReading == 0) {
            widget2.setTextReading("Current light: " + "no Light");
        } else if (currentReading <= 10) {
            widget2.setTextReading("Current light is dim: " + String.valueOf(currentReading));
        } else if (currentReading <= 70) {
            widget2.setTextReading("Current light is low: " + String.valueOf(currentReading));
        } else if (currentReading >= 70) {
            widget2.setTextReading("Current light is bright: " + String.valueOf(currentReading));
        }
    }


    public void interAcc() {

        float mLastX = widget2.mLastX;
        float mLastY = widget2.mLastY;
        float mLastZ = widget2.mLastZ;
        float x = 0;
        float y = 0;
        float z = 0;
        float Noise = widget2.Noise;

        float deltaX = Math.abs(mLastX - x);
        float deltaY = Math.abs(mLastY - y);
        float deltaZ = Math.abs(mLastZ - z);

        if (deltaX > deltaY && deltaX > Noise) {
            widget2.setImageView(R.drawable.x);
        } else if (deltaY > deltaX && deltaY > Noise) {
            widget2.setImageView(R.drawable.y);
        } else if (deltaZ > deltaY && deltaZ > Noise) {
            widget2.setImageView(R.drawable.z);
        } else {
            widget2.setImageView(R.drawable.white);
        }
    }

}
