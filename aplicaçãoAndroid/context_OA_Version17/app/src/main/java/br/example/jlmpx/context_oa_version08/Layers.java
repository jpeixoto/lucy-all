package br.example.jlmpx.context_oa_version08;

import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import br.example.jlmpx.lib.MapBuilder;
import br.example.jlmpx.lib.MarkerBuilder;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;

@SuppressWarnings("ALL")
public class Layers extends Activity implements SensorEventListener {

    //General variables
    private WebSocketClient mWebSocketClient;
    public String no_light = "0";
    public String bright = "70";
    public String message_received;
    public String lightSensor = "";
    public String accSensor = "";
    public String lightInter = "";
    public String accInter = "";
    public String flashlight = "";
    public String flashlight2 = "";
    public String flashlight3 = "";
    public String logCat = "";
    public String context_text = "";
    public String context_text2 = "";
    public String loc_text = "null";
    public String lum_options = "null";
    public String lum_options2 = "";
    public String lum_options3 = "0,0";
    public String acc_axis = "";
    public static String key = "";
    public boolean dialogShown = false;
    Button b1;

    //Acc variables
    private float mLastX, mLastY, mLastZ;
    private boolean mInitialized;
    private SensorManager mSensorManager;
    private Sensor mAccelerometer;
    private String Noise = "0.0";

    //Flashlight variables
    private Camera camera;
    private boolean isSupported_FEATURE_CAMERA_FLASH;
    MenuItem menuFlash;

    //LightSensor variables
    float currentReading;

    //GPS variables
    boolean gps_enabled = false;
    boolean network_enabled = false;
    double lat, lgt;
    String Text, Text2, loc_set = "";
    String loc_atual = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layers);
        connectWebSocket();

        ActionBar actionBar = getActionBar();
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME
                | ActionBar.DISPLAY_SHOW_TITLE | ActionBar.DISPLAY_SHOW_CUSTOM);

        //Enable GPS
        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        try {
            gps_enabled = manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
        }

        try {
            network_enabled = manager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex) {
        }

        if (!gps_enabled || !network_enabled) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(
                    "Your GPS seems to be disabled, do you want to enable it?")
                    .setCancelable(false)
                    .setPositiveButton("Yes",
                            new DialogInterface.OnClickListener() {
                                public void onClick(
                                        final DialogInterface dialog,
                                        final int id) {
                                    startActivity(new Intent(
                                            Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                                }
                            })
                    .setNegativeButton("No",
                            new DialogInterface.OnClickListener() {
                                public void onClick(
                                        final DialogInterface dialog,
                                        final int id) {
                                    dialog.cancel();
                                }
                            });
            final AlertDialog alert = builder.create();
            alert.show();
        }

        //Accelerometer startup
        mInitialized = false;
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);

        //LightSensor startup
        SensorManager sensorManager
                = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        Sensor lightSensor
                = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);

        if (lightSensor == null) {
            runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(Layers.this,
                            "No Light Sensor! quit-",
                            Toast.LENGTH_LONG).show();
                }
            });
        } else {
            sensorManager.registerListener(lightSensorEventListener,
                    lightSensor,
                    SensorManager.SENSOR_DELAY_NORMAL);
        }

        //Flashlight startup
        isSupported_FEATURE_CAMERA_FLASH = getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);

        //Getting sync code setting on initial view
        getKey();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        menuFlash = menu.findItem(R.id.flashlight);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.flashlight:
                if (isFlashLightOn()) {
                    turnFlashLightOff();
                } else {
                    turnFlashLightOn();
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    //Getting sync code setting on initial view
    public void getKey(){
        Bundle extras = getIntent().getExtras();
        if(extras != null){
            key = extras.getString("key");
            //System.out.println(key);
        }
    }

    //Loading GPS
    public class MyLocationListener implements LocationListener {
        @Override
        public void onLocationChanged(Location location) {
            TextView gps = (TextView) findViewById(R.id.gps);
            if (loc_atual.replaceAll("[^a-z]", "").equalsIgnoreCase("gpsyes") || loc_atual.replaceAll("[^a-z]", "").equalsIgnoreCase("locatual")) {
                gps.setText("Getting location...");
                lat = location.getLatitude();
                lgt = location.getLongitude();
                Text = "Latitude: " + lat + "\nLongitude: " + lgt;
                gps.setText(Text);
            }
            update();
        }

        @Override
        public void onProviderDisabled(String provider) {
            runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(getApplicationContext(), "Location Access Disabled",
                            Toast.LENGTH_SHORT).show();
                }
            });
        }

        @Override
        public void onProviderEnabled(String provider) {
            runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(getApplicationContext(), "Location Access Enabled",
                            Toast.LENGTH_SHORT).show();
                }
            });
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }

        public void update() {
            final ImageView image = (ImageView) findViewById(R.id.R1);
            image.post(new Runnable() {
                @Override
                public void run() {
                    int height = image.getMeasuredHeight();
                    int width = image.getMeasuredWidth();

                    if (loc_atual.replaceAll("[^a-z]", "").equalsIgnoreCase("locatual")) {
                        TextView gps = (TextView) findViewById(R.id.gps);
                        gps.setVisibility(View.INVISIBLE);
                        MapBuilder mapBuilder = new MapBuilder().center(lat, lgt).dimensions(width, height).zoom(17);
                        mapBuilder.setKey("AIzaSyAdKu5pZSP2l0xLkutrWYAQ70q-PyvVrJA");
                        int markerColor = getResources().getColor(R.color.default_map_color);
                        mapBuilder.addMarker(new MarkerBuilder().color(markerColor).position(lat, lgt));
                        String url = mapBuilder.build();

                        GetImageAsyncTask asyncTask = new GetImageAsyncTask();
                        asyncTask.execute(url);
                    } else if (loc_set.replaceAll("[^a-z]", "").equalsIgnoreCase("atong")) {
                        TextView gps = (TextView) findViewById(R.id.gps);
                        gps.setVisibility(View.INVISIBLE);
                        String coord = loc_set.replaceAll("[^0-9.,-]", "");
                        String[] coordArray = coord.split("\\s*,\\s*");

                        double lat2 = Double.parseDouble(coordArray[0]);
                        double lgt2 = Double.parseDouble(coordArray[1]);

                        Text2 = "Latitude: " + lat2 + "\nLongitude: " + lgt2;
                        Text = Text2;

                        MapBuilder mapBuilder = new MapBuilder().center(lat2, lgt2).dimensions(width, height).zoom(17);
                        mapBuilder.setKey("AIzaSyAdKu5pZSP2l0xLkutrWYAQ70q-PyvVrJA");
                        int markerColor = getResources().getColor(R.color.default_map_color);
                        mapBuilder.addMarker(new MarkerBuilder().color(markerColor).position(lat2, lgt2));
                        String url = mapBuilder.build();

                        GetImageAsyncTask asyncTask = new GetImageAsyncTask();
                        asyncTask.execute(url);
                    }
                }
            });
        }

        class GetImageAsyncTask extends AsyncTask<String, Void, Bitmap> {

            @Override
            protected Bitmap doInBackground(String... params) {
                try {
                    URL newUrl = new URL(params[0]);
                    Bitmap bitmap = BitmapFactory.decodeStream(newUrl.openConnection().getInputStream());
                    return bitmap;
                } catch (IOException e) {
                    e.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(Bitmap bitmap) {
                super.onPostExecute(bitmap);
                ImageView image = (ImageView) findViewById(R.id.R1);
                TextView gps = (TextView) findViewById(R.id.gps);
                if (loc_atual.replaceAll("[^a-z]", "").equalsIgnoreCase("locatual") ||
                        loc_atual.replaceAll("[^a-z]", "").equalsIgnoreCase("mapyes") ||
                        loc_set.replaceAll("[^a-z]", "").equalsIgnoreCase("atong")) {
                    image.setVisibility(View.VISIBLE);
                    gps.setText(Text);
                    image.setImageBitmap(bitmap);
                }else if (loc_atual.replaceAll("[^a-z]", "").equalsIgnoreCase("mapno")){
                    loc_set="";
                    image.setVisibility(View.INVISIBLE);
                    gps.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    //WebSocket startup
    public void connectWebSocket() {
        URI uri;
        try {
            uri = new URI("ws://138.197.6.216:8080");
        } catch (URISyntaxException e) {
            e.printStackTrace();
            return;
        }

        mWebSocketClient = new WebSocketClient(uri) {
            @Override
            public void onOpen(ServerHandshake serverHandshake) {
                Log.i("Websocket", "Opened");
            }

            //Get messages
            @Override
            public void onMessage(String s) {
                if (s.split(",")[0].replaceAll("[^a-z0-9]", "").equalsIgnoreCase(key)) {
                    String f = s.replace(s.split(",")[0] + ",", "");
                    message_received = f;

                    if (message_received.replaceAll("[^a-z]", "").equalsIgnoreCase("nolight")) {
                        no_light = message_received;
                    } else if (message_received.replaceAll("[^a-z]", "").equalsIgnoreCase("bright")) {
                        bright = message_received;
                    } else if (message_received.replaceAll("[^a-z]", "").equalsIgnoreCase("acc")) {
                        Noise = message_received;
                    } else if (message_received.replaceAll("[^a-z]", "").equalsIgnoreCase("atong")) {
                        loc_set = message_received;
                    } else if (message_received.replaceAll("[^0-9]", "").equalsIgnoreCase("1") ||
                            message_received.replaceAll("[^0-9]", "").equalsIgnoreCase("3") ||
                            message_received.replaceAll("[^0-9]", "").equalsIgnoreCase("20")) {
                        loc_atual = message_received;
                    } else if (message_received.replaceAll("[^0-9]", "").equalsIgnoreCase("4")) {
                        lightSensor = message_received;
                    } else if (message_received.replaceAll("[^0-9]", "").equalsIgnoreCase("5")) {
                        accSensor = message_received;
                    } else if (message_received.replaceAll("[^0-9]", "").equalsIgnoreCase("6")) {
                        lightInter = message_received;
                    } else if (message_received.replaceAll("[^0-9]", "").equalsIgnoreCase("7")) {
                        accInter = message_received;
                    } else if (message_received.replaceAll("[^0-9]", "").equalsIgnoreCase("8")) {
                        flashlight = message_received;
                    } else if (message_received.replaceAll("[^0-9]", "").equalsIgnoreCase("9") ||
                            message_received.replaceAll("[^0-9]", "").equalsIgnoreCase("10") ||
                            message_received.replaceAll("[^0-9]", "").equalsIgnoreCase("11")) {
                        logCat = message_received;
                    } else if (message_received.replaceAll("[^0-9]", "").equalsIgnoreCase("12")) {
                        context_text = message_received;
                    } else if (message_received.replaceAll("[^0-9]", "").equalsIgnoreCase("13")) {
                        loc_text = message_received;
                    } else if (message_received.replaceAll("[^0-9]", "").equalsIgnoreCase("14")) {
                        lum_options = message_received;
                    } else if ((message_received.split(",")[0]).equalsIgnoreCase("15")) {
                        lum_options3 = message_received;
                    } else if (message_received.replaceAll("[^0-9]", "").equalsIgnoreCase("16")) {
                        flashlight2 = message_received;
                    } else if (message_received.replaceAll("[^0-9]", "").equalsIgnoreCase("17")) {
                        context_text2 = message_received;
                    } else if (message_received.replaceAll("[^0-9]", "").equalsIgnoreCase("18")) {
                        flashlight3 = message_received;
                    } else if (message_received.replaceAll("[^0-9]", "").equalsIgnoreCase("19")) {
                        lum_options2 = message_received;
                    } else if (message_received.replaceAll("[^0-9]", "").equalsIgnoreCase("21")) {
                        acc_axis = message_received;
                    }
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        TextView gps = (TextView) findViewById(R.id.gps);
                        TextView textValue = (TextView) findViewById(R.id.light);
                        TextView textAcc = (TextView) findViewById(R.id.textAcc);
                        if(!logCat.isEmpty()) {
                            if (logCat.replaceAll("[^a-z]", "").equalsIgnoreCase("gpsenableyes")) {
                                gps.setVisibility(View.VISIBLE);
                                gps.setText("GPS Activated!");
                                logCat = "";
                            } else if (logCat.replaceAll("[^a-z]", "").equalsIgnoreCase("lightenableyes")) {
                                textValue.setVisibility(View.VISIBLE);
                                textValue.setText("Light Sensor Activated!");
                                logCat = "";
                            } else if (logCat.replaceAll("[^a-z]", "").equalsIgnoreCase("accenableyes")) {
                                textAcc.setVisibility(View.VISIBLE);
                                textAcc.setText("Accelerometer Activated!");
                                logCat = "";
                            } else if (logCat.replaceAll("[^a-z]", "").equalsIgnoreCase("gpsenableno")) {
                                gps.setVisibility(View.INVISIBLE);
                            } else if (logCat.replaceAll("[^a-z]", "").equalsIgnoreCase("accenableno")) {
                                textAcc.setVisibility(View.INVISIBLE);
                                textAcc.setText("");
                                logCat = "";
                            } else if (logCat.replaceAll("[^a-z]", "").equalsIgnoreCase("lightenableno")) {
                                textValue.setVisibility(View.INVISIBLE);
                                textValue.setText("");
                                logCat = "";
                            }
                        }
                        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                        LocationListener mlocListener = new MyLocationListener();
                        Toast.makeText(getApplicationContext(), "Locating...", Toast.LENGTH_LONG);
                        manager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 1, mlocListener);

                        messageGPS();
                    }
                });
            }

            public void messageGPS() {
                ImageView image = (ImageView) findViewById(R.id.R1);
                TextView gps = (TextView) findViewById(R.id.gps);
                if (loc_atual.replaceAll("[^a-z]", "").equalsIgnoreCase("gpsno")) {
                    gps.setText("GPS Activated!");
                } else if (loc_atual.replaceAll("[^a-z]", "").equalsIgnoreCase("mapno")) {
                    loc_set="";
                    image.setVisibility(View.INVISIBLE);
                    gps.setVisibility(View.VISIBLE);
                } else if (loc_atual.replaceAll("[^a-z]", "").equalsIgnoreCase("uadasombras")) {
                    loc_set = "";
                    gps.setText("Shadow Street");
                    image.setVisibility(View.VISIBLE);
                    image.setImageResource(R.drawable.r1);
                    gps.setVisibility(View.INVISIBLE);
                } else if (loc_atual.replaceAll("[^a-z]", "").equalsIgnoreCase("uadoorto")) {
                    loc_set = "";
                    gps.setText("Porto Street");
                    image.setVisibility(View.VISIBLE);
                    image.setImageResource(R.drawable.r2);
                    gps.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onClose(int i, String s, boolean b) {
                runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(getApplicationContext(), "Connection lost. Re-enter a key, please.",
                                Toast.LENGTH_LONG).show();
                    }
                });
                Log.i("Websocket", "Closed " + s);
            }

            @Override
            public void onError(Exception e) {
                Log.i("Websocket", "Error " + e.getMessage());
            }
        };
        mWebSocketClient.connect();
    }

    //Delimitation of the LightSensor
    SensorEventListener lightSensorEventListener
            = new SensorEventListener() {

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onSensorChanged(SensorEvent event) {
            currentReading = event.values[0];
            // TODO Auto-generated method stub
            if (event.sensor.getType() == Sensor.TYPE_LIGHT) {
                TextView textReading = (TextView) findViewById(R.id.reading);
                TextView textValue = (TextView) findViewById(R.id.light);
                ProgressBar lightMeter = (ProgressBar) findViewById(R.id.lightmeter);
                lightMeter.setProgress((int) currentReading);

                if (lightSensor.replaceAll("[^a-z]", "").equalsIgnoreCase("lightyes")) { //light sensor widgets information
                    textValue.setText("Light sensor: " + String.valueOf(currentReading));
                    if (lightInter.replaceAll("[^a-z]", "").equalsIgnoreCase("lightnteryes")) { //light sensor interpreter information
                        textValue.setVisibility(View.INVISIBLE);
                        lightMeter.setVisibility(View.VISIBLE);
                        textReading.setVisibility(View.VISIBLE);
                        if (currentReading <= Integer.parseInt(no_light.replaceAll("[^0-9]", ""))) {
                            textReading.setText("Current light: " + "no light");
                        } else if (currentReading >= Integer.parseInt(bright.replaceAll("[^0-9]", ""))) {
                            textReading.setText("Current light is bright: " + String.valueOf(currentReading));
                        } else if (currentReading <= 10) {
                            textReading.setText("Current light is dim: " + String.valueOf(currentReading));
                        } else if (currentReading <= 70) {
                            textReading.setText("Current light is low: " + String.valueOf(currentReading));
                        }
                    } else {
                        textReading.setVisibility(View.INVISIBLE);
                        lightMeter.setVisibility(View.INVISIBLE);
                        textValue.setVisibility(View.VISIBLE);
                    }
                } else if(lightSensor.replaceAll("[^a-z]", "").equalsIgnoreCase("lightno")){
                    textValue.setText("Light Sensor Activated!");
                }

                //Alert feature aggregator
                if (loc_text.replaceAll("[^a-zA-Z]", "").equals(loc_atual.replaceAll("[^a-zA-Z]", ""))) {
                    if ((lum_options.replaceAll("[^a-zA-Z]", "").equals("noLight") && (currentReading <= Integer.parseInt(no_light.replaceAll("[^0-9]", ""))) ||
                            (lum_options.replaceAll("[^a-zA-Z]", "").equals("Bright") && currentReading >= Integer.parseInt(bright.replaceAll("[^0-9]", ""))) ||
                            (lum_options.replaceAll("[^a-zA-Z]", "").equals("Low") && currentReading <= 70) ||
                            (lum_options.replaceAll("[^a-zA-Z]", "").equals("Dim") && currentReading <= 10))) {
                        if (!dialogShown) {
                            final AlertDialog alertDialog = new AlertDialog.Builder(Layers.this).create();
                            alertDialog.setCanceledOnTouchOutside(false);
                            alertDialog.setTitle(context_text.replaceAll("[^a-zA-Z ]", "") + " place detected!");
                            alertDialog.setMessage("Contextual information detected according to your settings."+ "\n");
                            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialogShown = false;
                                            dialog.dismiss();
                                        }
                                    });
                            dialogShown = true;
                            alertDialog.show();
                        }
                    }
                }

                //Simple context trigger
                if (loc_text.replaceAll("[^a-zA-Z]", "").equals(loc_atual.replaceAll("[^a-zA-Z]", ""))) {
                    if ((lum_options.replaceAll("[^a-zA-Z]", "").equals("noLight") && (currentReading <= Integer.parseInt(no_light.replaceAll("[^0-9]", ""))) ||
                            (lum_options.replaceAll("[^a-zA-Z]", "").equals("Bright") && currentReading >= Integer.parseInt(bright.replaceAll("[^0-9]", ""))) ||
                            (lum_options.replaceAll("[^a-zA-Z]", "").equals("Low") && currentReading <= 70) ||
                            (lum_options.replaceAll("[^a-zA-Z]", "").equals("Dim") && currentReading <= 10)) && context_text2.replaceAll("[^a-zA-Z]", "").equals(context_text.replaceAll("[^a-zA-Z]", ""))
                            && flashlight3.replaceAll("[^a-z]", "").equals("enableflashlightyes")) {
                        turnFlashLightOn();
                    } else if ((lum_options.replaceAll("[^a-zA-Z]", "").equals("noLight") && (currentReading <= Integer.parseInt(no_light.replaceAll("[^0-9]", ""))) ||
                            (lum_options.replaceAll("[^a-zA-Z]", "").equals("Bright") && currentReading >= Integer.parseInt(bright.replaceAll("[^0-9]", ""))) ||
                            (lum_options.replaceAll("[^a-zA-Z]", "").equals("Low") && currentReading <= 70) ||
                            (lum_options.replaceAll("[^a-zA-Z]", "").equals("Dim") && currentReading <= 10)) && context_text2.replaceAll("[^a-zA-Z]", "").equals(context_text.replaceAll("[^a-zA-Z]", ""))
                            && flashlight3.replaceAll("[^a-z]", "").equals("enableflashlightno")) {
                        turnFlashLightOff();
                    }
                }

                //Simple light trigger
                if (Integer.parseInt(lum_options3.split(",")[1]) == currentReading &&
                        flashlight2.replaceAll("[^a-z]", "").equals("enableflashlightyes")) {
                    turnFlashLightOn();
                } else if (Integer.parseInt(lum_options3.split(",")[1]) == currentReading &&
                        flashlight2.replaceAll("[^a-z]", "").equals("enableflashlightno")) {
                    turnFlashLightOff();
                }
            }
        }
    };

    //Delimitation of the Accelerometer
    @TargetApi(Build.VERSION_CODES.CUPCAKE)
    protected void onResume() {
        super.onResume();
        mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @TargetApi(Build.VERSION_CODES.CUPCAKE)
    protected void onPause() {
        super.onPause();
        mSensorManager.unregisterListener(this);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        // can be safely ignored for this demo
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        TextView tvX = (TextView) findViewById(R.id.x_axis);
        TextView tvY = (TextView) findViewById(R.id.y_axis);
        TextView tvZ = (TextView) findViewById(R.id.z_axis);
        TextView tvX1 = (TextView) findViewById(R.id.x_axis1);
        TextView tvY1 = (TextView) findViewById(R.id.y_axis1);
        TextView tvZ1 = (TextView) findViewById(R.id.z_axis1);
        ImageView iv = (ImageView) findViewById(R.id.image);
        TextView textAcc = (TextView) findViewById(R.id.textAcc);
        TableLayout table = (TableLayout) findViewById(R.id.tab);
        TableLayout table1 = (TableLayout) findViewById(R.id.tab1);

        float x = event.values[0];
        float y = event.values[1];
        float z = event.values[2];

        if (accSensor.replaceAll("[^a-z]", "").equalsIgnoreCase("accyes")) {
            textAcc.setVisibility(View.INVISIBLE);
            table1.setVisibility(View.VISIBLE);
            if (!mInitialized) {
                mLastX = x;
                mLastY = y;
                mLastZ = z;
                tvX.setText("0.0");
                tvY.setText("0.0");
                tvZ.setText("0.0");
                tvX1.setText("0.0");
                tvY1.setText("0.0");
                tvZ1.setText("0.0");
                mInitialized = true;
            } else {
                float deltaX = Math.abs(mLastX - x);
                float deltaY = Math.abs(mLastY - y);
                float deltaZ = Math.abs(mLastZ - z);
                if (deltaX < Float.valueOf(Noise.replaceAll("[^0-9.]", "")).floatValue())
                    deltaX = (float) 0.0;
                if (deltaY < Float.valueOf(Noise.replaceAll("[^0-9.]", "")).floatValue())
                    deltaY = (float) 0.0;
                if (deltaZ < Float.valueOf(Noise.replaceAll("[^0-9.]", "")).floatValue())
                    deltaZ = (float) 0.0;
                mLastX = x;
                mLastY = y;
                mLastZ = z;
                tvX.setText(String.format("%.2f", deltaX));
                tvY.setText(String.format("%.2f", deltaY));
                tvZ.setText(String.format("%.2f", deltaZ));
                tvX1.setText(String.format("%.2f", deltaX));
                tvY1.setText(String.format("%.2f", deltaY));
                tvZ1.setText(String.format("%.2f", deltaZ));

                if (accInter.replaceAll("[^a-z]", "").equalsIgnoreCase("accnteryes")) {
                    table1.setVisibility(View.INVISIBLE);
                    table.setVisibility(View.VISIBLE);
                    iv.setVisibility(View.VISIBLE);
                    if (deltaX > deltaY && deltaX > Float.valueOf(Noise.replaceAll("[^0-9.]", "")).floatValue()) {
                        iv.setImageResource(R.drawable.x);
                    } else if (deltaY > deltaX && deltaY > Float.valueOf(Noise.replaceAll("[^0-9.]", "")).floatValue()) {
                        iv.setImageResource(R.drawable.y);
                    } else if (deltaZ > deltaY && deltaZ > Float.valueOf(Noise.replaceAll("[^0-9.]", "")).floatValue()) {
                        iv.setImageResource(R.drawable.z);
                    } else {
                        iv.setVisibility(View.INVISIBLE);
                    }
                } else {
                    table.setVisibility(View.INVISIBLE);
                    table1.setVisibility(View.VISIBLE);
                    iv.setVisibility(View.INVISIBLE);
                }

                //Enable complex trigger flashlight
                if ((lum_options2.replaceAll("[^a-zA-Z]", "").equals("noLighttwo") && (currentReading <= Integer.parseInt(no_light.replaceAll("[^0-9]", ""))) ||
                        (lum_options2.replaceAll("[^a-zA-Z]", "").equals("Brighttwo") && currentReading >= Integer.parseInt(bright.replaceAll("[^0-9]", ""))) ||
                        (lum_options2.replaceAll("[^a-zA-Z]", "").equals("Lowtwo") && currentReading <= 70) ||
                        (lum_options2.replaceAll("[^a-zA-Z]", "").equals("Dimtwo") && currentReading <= 10)) &&
                        (acc_axis.replaceAll("[^a-zA-Z]", "").equals("eixox") && deltaX > Float.valueOf(Noise.replaceAll("[^0-9.]", "")).floatValue() ||
                                acc_axis.replaceAll("[^a-zA-Z]", "").equals("eixoy") && deltaY > Float.valueOf(Noise.replaceAll("[^0-9.]", "")).floatValue() ||
                                acc_axis.replaceAll("[^a-zA-Z]", "").equals("eixoz") && deltaZ > Float.valueOf(Noise.replaceAll("[^0-9.]", "")))) {
                    if (flashlight.replaceAll("[^a-z]", "").equals("enableflashlightyes")) {
                        turnFlashLightOn();
                    } else if (flashlight.replaceAll("[^a-z]", "").equals("enableflashlightno")) {
                        turnFlashLightOff();
                    }
                }
            }
        } else if(accSensor.replaceAll("[^a-z]", "").equalsIgnoreCase("accno") && !logCat.replaceAll("[^a-z]", "").equalsIgnoreCase("accenableno")) {
            table1.setVisibility(View.INVISIBLE);
            textAcc.setVisibility(View.VISIBLE);
        }
    }


    private boolean isFlashLightOn() {
        Camera.Parameters cameraParam = camera.getParameters();
        return cameraParam.getFlashMode().equals(cameraParam.FLASH_MODE_TORCH);
    }

    private void turnFlashLightOn() {
        if (isSupported_FEATURE_CAMERA_FLASH) {
            Camera.Parameters cameraParam = camera.getParameters();
            List<String> flashModes = cameraParam.getSupportedFlashModes();
            if (flashModes != null && flashModes.contains(cameraParam.FLASH_MODE_TORCH)) {
                cameraParam.setFlashMode(cameraParam.FLASH_MODE_TORCH);
                camera.setParameters(cameraParam);
                menuFlash.setIcon(R.drawable.on);
            } else {
                runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(getApplicationContext(), R.string.camera_flash_not_supported, Toast.LENGTH_LONG);
                    }
                });
            }
        } else {
            runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(getApplicationContext(), R.string.camera_flash_not_supported, Toast.LENGTH_LONG);
                }
            });
        }
    }

    private void turnFlashLightOff() {
        if (isSupported_FEATURE_CAMERA_FLASH) {
            Camera.Parameters cameraParam = camera.getParameters();
            List<String> flashModes = cameraParam.getSupportedFlashModes();
            if (flashModes != null && flashModes.contains(cameraParam.FLASH_MODE_OFF)) {
                cameraParam.setFlashMode(cameraParam.FLASH_MODE_OFF);
                camera.setParameters(cameraParam);
                menuFlash.setIcon(R.drawable.off);
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        camera = Camera.open();
        camera.startPreview();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (isFlashLightOn()) {
            turnFlashLightOff();
        }
        camera.stopPreview();
        camera.release();
        camera = null;
    }
}
