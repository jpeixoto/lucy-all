package br.example.jlmpx.context_oa_version08;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


@SuppressWarnings("ALL")
public class MainActivity extends Activity  {

    //General variables
    public static String key = "";
    Button b1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment())
                    .commit();
        }

    }

    //A placeholder fragment containing a simple view.
    public class PlaceholderFragment extends Fragment {

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            return rootView;
        }

        @Override
        public void onViewCreated(final View rootView, Bundle savedInstanceState) {
            super.onViewCreated(rootView, savedInstanceState);
            Button b1 = (Button) rootView.findViewById(R.id.ok);
            final EditText textViewKey = (EditText) rootView.findViewById(R.id.key);

            b1.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (textViewKey.getText().toString().trim().length() == 0) {
                        Toast.makeText(MainActivity.this, "Input text is empty.. please enter a key.", Toast.LENGTH_LONG).show();
                    } else {
                        Intent intent = new Intent(MainActivity.this, Layers.class);
                        EditText textViewKey = (EditText) findViewById(R.id.key);
                        key = textViewKey.getText().toString();
                        Bundle extras = new Bundle();
                        extras.putString("key", key);
                        intent.putExtras(extras);
                        startActivity(intent);
                    }
                }
            });
        }
    }
}
